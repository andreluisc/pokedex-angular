import { Injectable } from '@angular/core';
import { MatPaginatorIntl } from '@angular/material/paginator';
import { TranslocoService } from '@ngneat/transloco';

@Injectable()
export class CustomPaginatorIntl extends MatPaginatorIntl {
  constructor(private transloco: TranslocoService) {
    super();
    this.getAndInitTranslations();
  }

  getAndInitTranslations() {
    this.transloco
      .selectTranslate('matPaginator.itemsPerPageLabel')
      .subscribe((result) => {
        this.itemsPerPageLabel = result;
        this.changes.next();
      });
    this.transloco
      .selectTranslate('matPaginator.nextPageLabel')
      .subscribe((result) => {
        this.nextPageLabel = result;
        this.changes.next();
      });
    this.transloco
      .selectTranslate('matPaginator.previousPageLabel')
      .subscribe((result) => {
        this.previousPageLabel = result;
        this.changes.next();
      });
    this.transloco
      .selectTranslate('matPaginator.firstPageLabel')
      .subscribe((result) => {
        this.firstPageLabel = result;
        this.changes.next();
      });
    this.transloco
      .selectTranslate('matPaginator.lastPageLabel')
      .subscribe((result) => {
        this.lastPageLabel = result;
        this.changes.next();
      });
  }

  override getRangeLabel = (page: number, pageSize: number, length: number) => {
    if (length === 0 || pageSize === 0) {
      return `0 de ${length}`;
    }
    const start = page * pageSize + 1;
    const end = (page + 1) * pageSize > length ? length : (page + 1) * pageSize;
    return this.transloco.translate('matPaginator.getRangeLabel', {
      start,
      end,
      length,
    });
  };
}
