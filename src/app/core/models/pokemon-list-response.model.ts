import { Pokemon } from './pokemon.model';

/**
 * @interface
 * Representa a resposta de uma lista de Pokémon, incluindo informações sobre a contagem,
 * URLs para a próxima e a página anterior, e uma matriz de objetos Pokemon.
 */
export interface PokemonListResponse {
  /**
   * O número total de Pokémon na lista.
   */
  count: number;

  /**
   * A URL da próxima página de resultados ou null se não houver próxima página.
   */
  next: string | null;

  /**
   * A URL da página anterior de resultados ou null se não houver página anterior.
   */
  previous: string | null;

  /**
   * Uma matriz de objetos Pokemon que contém informações sobre cada Pokémon na lista.
   */
  results: Pokemon[];
}
