import { createReducer, on } from '@ngrx/store';
import { initialPokemonState } from '../../models/pokemon-state.model';
import * as PokemonActions from '../actions/pokemon.actions';

// Definindo o reducer para o state dos pokémons.
export const pokemonReducer = createReducer(
  // State inicial definido em initialPokemonState.
  initialPokemonState,

  // Atualiza o estado de carregamento.
  on(PokemonActions.loadPokemons, (state) => ({ ...state, loading: true })),

  // Atualiza o estado com os dados carregados.
  on(PokemonActions.loadPokemonsSuccess, (state, { data }) => ({
    ...state,
    data,
    loaded: true,
    loading: false,
  })),

  // Trata falhas no carregamento.
  on(PokemonActions.loadPokemonsFailure, (state, { error }) => ({
    ...state,
    error,
    loaded: false,
    loading: false,
  })),

  on(PokemonActions.loadPokemonDetailsSuccess, (state, { detailData }) => {
    return {
      ...state,
      pokemonDetails: {
        ...state.pokemonDetails,
        [detailData.id]: detailData
      }
    };
  })
);
