# Use official node image as the base image
FROM node:16-alpine

# Set the working directory
WORKDIR /usr/local/app

# Copy package.json and package-lock.json for caching purposes
COPY package*.json ./

# Install global Angular CLI
RUN npm install -g @angular/cli

# Install all the dependencies
RUN npm install

# Add the source code to app
COPY ./ ./

# Start the app with hot-reloading enabled
CMD ["npm", "start"]

#docker build -t pokedex:dev .
#docker run -v ${PWD}:/usr/local/app -p 4201:4200 --rm pokedex:dev
# docker run -v ${PWD}:/app -v /app/node_modules -p 4201:4200 --rm example:dev
