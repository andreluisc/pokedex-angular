import { ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { CustomMaterialModule } from '../../../core/shared/custom-material.module';

/**
 * Importe o componente que vai ser testado e os models relacionados
 */
import { AsyncImageComponent } from './async-image.component';
import { MatProgressSpinner } from '@angular/material/progress-spinner';

/**
 * Conjunto de testes do AsyncImageComponent
 */
describe('AsyncImageComponent', () => {
  let component: AsyncImageComponent;
  let fixture: ComponentFixture<AsyncImageComponent>;

  /**
   * Configura e prepara o ambiente de teste antes de cada teste
   */
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [AsyncImageComponent],
      imports: [CustomMaterialModule],
    }).compileComponents();

    fixture = TestBed.createComponent(AsyncImageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  /**
   * Teste: Verifica se o componente foi criado com sucesso.
   */
  it('componente deve ser criado', () => {
    expect(component).toBeTruthy();
  });

  /**
   * Teste: verifica se o spinner é exibido inicialmente.
   */
  it('deve exibir o spinner inicialmente', () => {
    const spinnerElem = fixture.debugElement.query(
      By.directive(MatProgressSpinner)
    );
    expect(spinnerElem).toBeTruthy();
  });

  /**
   * Teste: verifica se o spinner é ocultado e a imagem é exibida quando a imagem é carregada.
   */
  it('deve ocultar o spinner e exibir a imagem quando a imagem é carregada', () => {
    const imgElem = fixture.debugElement.query(By.css('img'));
    imgElem.triggerEventHandler('load', null);

    fixture.detectChanges();

    const updatedSpinnerElem = fixture.debugElement.query(
      By.directive(MatProgressSpinner)
    );
    expect(updatedSpinnerElem).toBeNull();

    expect(imgElem.nativeElement.classList.contains('visible')).toBe(true);
    expect(imgElem.nativeElement.classList.contains('invisible')).toBe(false);
  });
});
