import {
  ComponentFixture,
  TestBed,
  fakeAsync,
  tick,
} from '@angular/core/testing';

import { provideMockStore, MockStore } from '@ngrx/store/testing';
import { PokemonListComponent } from './pokemon-list.component';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatPaginatorModule } from '@angular/material/paginator';
import { Pipe, PipeTransform } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {PokemonFilterComponent} from "./components/pokemon-filter/pokemon-filter.component";
// import {Subscription, TeardownLogic} from 'rxjs';
// import Exclude from "$GLOBAL$";
// import jest from "$GLOBAL$";

/**
 * Implementação simulada do filtro pelo pipe filterByName
 */
@Pipe({ name: 'filterByName' })
class MockFilterByNamePipe implements PipeTransform {
  transform(value: unknown): unknown {
    return value;
  }
}

/**
 * Conjunto de testes o PokemonListComponent
 */
describe('PokemonListComponent', () => {
  let component: PokemonListComponent;
  let fixture: ComponentFixture<PokemonListComponent>;
  let store: MockStore;

  // Define o estado inicial
  const initialState = {
    pokemon: {
      data: [],
      loading: false,
      error: null,
    },
  };
  /**
   * Configura e prepara o ambiente de teste antes de cada teste
   */
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        MatGridListModule,
        MatPaginatorModule,
        BrowserAnimationsModule, // Adicione esta linha
      ],
      declarations: [PokemonListComponent, MockFilterByNamePipe, PokemonFilterComponent ],
      providers: [provideMockStore({ initialState })],
    }).compileComponents();

    store = TestBed.inject(MockStore);
    fixture = TestBed.createComponent(PokemonListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  /**
   * Teste: verifica se o componente é criado corretamente.
   */
  it('deve criar o componente', () => {
    expect(component).toBeTruthy();
  });

  /**
   * Teste: verifica se o componente inicializa com as propriedades padrão corretas.
   */
  it('deve inicializar com propriedades padrão corretas', () => {
    expect(component.filter).toBe('');
    expect(component.pokemons).toEqual([]);
    expect(component.displayedPokemons).toEqual([]);
    expect(component.pageSize).toBe(9);
    expect(component.isLoading).toBe(false);
    expect(component.error).toBe(false);
  });

  /**
   * Teste: verifica se a propriedade isLoading é atualizada quando o estado da loja emite o carregamento.
   */
  it('deve atualizar a propriedade isLoading quando o estado da loja emite carregamento', () => {
    store.setState({
      pokemon: {
        data: [],
        loading: true,
        error: null,
      },
    });

    expect(component.isLoading).toBe(true);
  });

  /**
   * Teste: verifica se a propriedade de erro é atualizada quando o estado da loja emite erro.
   */
  it('deve atualizar a propriedade de erro quando o estado da loja emite erro', () => {
    store.setState({
      pokemon: {
        data: [],
        loading: false,
        error: new Error('Algum erro'),
      },
    });

    expect(component.error).toBe(true);
  });

  /**
   * Teste: verifica se os dados do Pokémon são processados e mapeados corretamente quando o estado da loja emite dados.
   */
  it('deve processar e mapear corretamente os dados do Pokémon quando o estado da loja emite dados', fakeAsync(() => {
    const mockPokemons = {
      results: [
        { name: 'Bulbasaur', url: 'https://pokeapi.co/api/v2/pokemon/1/' },
      ],
    };

    store.setState({
      pokemon: {
        data: mockPokemons,
        loading: false,
        error: null,
      },
    });

    tick(); // Simula a passagem do tempo

    expect(component.pokemons.length).toBe(1);
    expect(component.pokemons[0].id).toBe(1);
    expect(component.pokemons[0].sprite).toBeDefined();
  }));

  /**
   * Teste: verifica se displayedPokemons é atualizado na mudança de página.
   */
  it('deve atualizar displayedPokemons na mudança de página', () => {
    const mockPokemons = Array.from({ length: 20 }, (_, i) => {
      return {
        id: 17,
        name: `Pokemon${i + 1}`,
        url: `https://pokeapi.co/api/v2/pokemon/${i + 1}/`,
      };
    });

    store.setState({
      pokemon: {
        data: mockPokemons,
        loading: false,
        error: null,
      },
    });

    component.pokemons = mockPokemons;

    component.pageChanged({
      pageIndex: 1,
      pageSize: 9,
      length: mockPokemons.length,
    });

    expect(component.displayedPokemons.length).toBe(9);
    expect(component.displayedPokemons[0].name).toBe('Pokemon10');
  });
});
