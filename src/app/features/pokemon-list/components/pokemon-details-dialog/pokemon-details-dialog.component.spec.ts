import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { By } from '@angular/platform-browser';
import { PokemonDetailsDialogComponent } from './pokemon-details-dialog.component';
import { CapitalizePipe } from "../../../../shared/pipes/capitalize/capitalize.pipe";
import { Component, Input } from "@angular/core";
import { getTranslocoModule } from '../../../../../testing/transloco-testing.module'

// Mock do componente AsyncImage
@Component({
  selector: 'app-async-image',
  template: '<img src="{{src}}" alt="{{alt}}">'
})
class MockAsyncImageComponent {
  @Input() src!: string;
  @Input() alt!: string;
}

describe('PokemonDetailsDialogComponent', () => {
  let component: PokemonDetailsDialogComponent;
  let fixture: ComponentFixture<PokemonDetailsDialogComponent>;
  const mockData = {
    pokemonDetails: {
      name: 'bulbasaur',
      sprite: 'sprite_url',
      height: 1,
      weight: 5,
      abilities: [
        { name: 'ability1' },
        { name: 'ability2' }
      ],
      moves: [
        { name: 'move1' },
        { name: 'move2' },
        { name: 'move3' },
        { name: 'move4' }
      ],
      stats: [
        { stat: 50, name: 'hp' },
        { stat: 60, name: 'attack' }
      ]
    }
  };

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [PokemonDetailsDialogComponent, MockAsyncImageComponent, CapitalizePipe],
      imports: [getTranslocoModule()],
      providers: [
        { provide: MAT_DIALOG_DATA, useValue: mockData },
      ]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PokemonDetailsDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    jest.clearAllMocks();
  });

  /**
   * teste: Verifica se o componente foi criado com sucesso
   */
  it('deve ser criado', () => {
    expect(component).toBeTruthy();
  });

  /**
   * teste: Verifica se o nome do Pokémon é exibido
   */
  it('deve exibir o nome do Pokémon', () => {
    const nameElem = fixture.debugElement.query(By.css('.pokemon-detail__name'));
    expect(nameElem.nativeElement.textContent).toContain('Bulbasaur');
  });

  /**
   * teste: Verifica se o sprite do Pokémon é exibido corretamente
   */
  it('deve exibir o sprite do Pokémon', () => {
    const imageElem = fixture.debugElement.query(By.css('app-async-image img')).nativeElement;
    expect(imageElem.getAttribute('src')).toEqual('sprite_url');
    expect(imageElem.getAttribute('alt')).toEqual('bulbasaur');
  });

  /**
   * teste: Verifica se a altura e o peso do Pokémon são exibidos corretamente
   */
  it('deve exibir a altura e o peso do Pokémon', () => {
    const heightElem = fixture.debugElement.query(By.css('.pokemon-detail__about__data__item:nth-child(1)'));
    expect(heightElem.nativeElement.textContent).toContain('1m');

    const weightElem = fixture.debugElement.query(By.css('.pokemon-detail__about__data__item:nth-child(2)'));
    expect(weightElem.nativeElement.textContent).toContain('5kg');
  });

  /**
   * teste: Verifica se as habilidades do Pokémon são exibidas
   */
  it('deve exibir as habilidades do Pokémon', () => {
    const abilitiesElem = fixture.debugElement.query(By.css('.pokemon-detail__about__data__item:nth-child(3)'));
    expect(abilitiesElem.nativeElement.textContent).toContain('ability1');
    expect(abilitiesElem.nativeElement.textContent).toContain('ability2');
  });

  /**
   * teste: Verifica se os primeiros 3 movimentos do Pokémon são exibidos
   */
  it('deve exibir os primeiros 3 movimentos do Pokémon', () => {
    const movesElem = fixture.debugElement.query(By.css('.pokemon-detail__about__data__item:nth-child(4)'));
    expect(movesElem.nativeElement.textContent).toContain('move1');
    expect(movesElem.nativeElement.textContent).toContain('move2');
    expect(movesElem.nativeElement.textContent).toContain('move3');
    expect(movesElem.nativeElement.textContent).not.toContain('move4');
  });

  /**
   * teste: Verifica se os stats do Pokémon são exibidos corretamente
   */
  it('deve exibir os stats do Pokémon', () => {
    const statsElems = fixture.debugElement.queryAll(By.css('.pokemon-detail__stats__items__item'));

    expect(statsElems[0].nativeElement.textContent).toContain('50');
    expect(statsElems[0].nativeElement.textContent).toContain('HP');

    expect(statsElems[1].nativeElement.textContent).toContain('60');
    expect(statsElems[1].nativeElement.textContent).toContain('Ataque');
  });
});
