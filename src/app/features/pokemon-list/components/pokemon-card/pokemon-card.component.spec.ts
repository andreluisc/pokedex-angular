// Importe os módulos e componentes de teste necessários
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatExpansionModule } from "@angular/material/expansion";
import { MatDialogModule } from "@angular/material/dialog";

import { Component, Input } from '@angular/core';

// Importe o componente que vai ser testado e os models relacionados
import { PokemonCardComponent } from './pokemon-card.component';
import { Pokemon } from '../../../../core/models/pokemon.model';
import { provideMockStore } from '@ngrx/store/testing';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { CapitalizePipe } from "../../../../shared/pipes/capitalize/capitalize.pipe";

/**
 * Crie um componente falso para carregamento de imagens assíncronas
 */

@Component({
  selector: 'app-async-image',
  template: '<img [src]="src" alt="{{alt}}">',
})
class MockAsyncImageComponent {
  @Input() src!: string;
  @Input() alt!: string;
}

/**
 * Conjunto de testes o PokemonCardComponent
 */
describe('PokemonCardComponent', () => {
  let component: PokemonCardComponent;
  let fixture: ComponentFixture<PokemonCardComponent>;
  /**
   * Configura e prepara o ambiente de teste antes de cada teste
   */
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [PokemonCardComponent, MockAsyncImageComponent, CapitalizePipe],
      imports: [MatGridListModule, MatExpansionModule, NoopAnimationsModule, MatDialogModule],
      providers: [
        provideMockStore({ initialState: {} })
      ]
    }).compileComponents();

    fixture = TestBed.createComponent(PokemonCardComponent);
    component = fixture.componentInstance;
  });

  /**
   * Teste: Verifica se o componente foi criado com sucesso
   */
  it('componente deve ser criado', () => {
    expect(component).toBeTruthy();
  });

  /**
   * Teste: Verifica se o nome do Pokemon está sendo exibido corretamente
   */
  it('deve exibir o nome do Pokemon', () => {
    // Defina os dados do Pokemon para o componente
    component.pokemon = {
      id: 17,
      name: 'Pikachu',
      url: 'https://pokeapi.co/pokemon/25/',
    };
    // Acione a detecção de mudanças
    fixture.detectChanges();

    // Encontre e valide o elemento de exibição do nome do Pokemon
    const headerElement = fixture.nativeElement.querySelector('.pokemon-name');
    expect(headerElement.textContent).toContain('Pikachu');
  });

  /**
   * Teste: Verifica se o componente async-image exibe os atributos src e alt corretos
   */
  it('deve exibir async-image com src e alt corretos', () => {
    // Crie um objeto Pokemon
    const pokemon: Pokemon = {
      id: 17,
      name: 'Pikachu',
      sprite: 'https://some-img-url.com/pikachu.png',
      url: 'https://pokeapi.co/pokemon/25/',
    };

    // Defina os dados do Pokemon para o componente
    component.pokemon = pokemon;
    // Acione a detecção de mudanças
    fixture.detectChanges();

    // Encontre o elemento async-image usando debugElement e valide seus atributos
    const asyncImageElem = fixture.debugElement.query(
      By.directive(MockAsyncImageComponent)
    );
    expect(asyncImageElem.componentInstance.src).toEqual(pokemon.sprite);
    expect(asyncImageElem.componentInstance.alt).toEqual(pokemon.name);
  });
});
