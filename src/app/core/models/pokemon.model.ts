/**
 * @class
 * Esta classe representa um objeto Pokémon com suas propriedades básicas.
 * Pode ser usada para armazenar informações sobre um Pokémon, como seu nome, ID, sprite e URL.
 */
export class Pokemon {
  /**
   * O ID do Pokémon (opcional).
   */
  id: number;

  /**
   * O sprite (imagem) do Pokémon (opcional).
   */
  sprite?: string;

  /**
   * O nome do Pokémon (opcional).
   */
  name?: string;

  /**
   * A URL associada ao Pokémon (obrigatória).
   */
  url: string;

  /**
   * Construtor da classe, inicializa as propriedades com valores padrão.
   */
  constructor() {
    this.id = 0;
    this.name = '';
    this.url = '';
  }
}
