import { createAction, props } from '@ngrx/store';

// Importar o modelo PokemonListResponse para ser usado na action
import { PokemonListResponse } from '../../models/pokemon-list-response.model';
import { PokemonDetail } from "../../models/pokemon-detail.model";

// Iniciar o carregamento dos pokémons
export const loadPokemons = createAction('[Pokemon] Load Pokemons');

// Action disparada em caso de sucesso no carregamento dos pokémons
export const loadPokemonsSuccess = createAction(
  '[Pokemon] Load Pokemons Success',
  // Usando props para especificar o formato dos dados associados à ação.
  props<{ data: PokemonListResponse }>()
);

// Em caso de falha no carregamento de pokémons.
export const loadPokemonsFailure = createAction(
  '[Pokemon] Load Pokemons Failure',
  // Usando props para especificar o formato dos erros associados à ação.
  props<{ error: unknown }>()
);

export const loadPokemonDetails = createAction(
  '[Pokemon] Load Pokemon Details',
  props<{ id: number }>()
);

export const loadPokemonDetailsSuccess = createAction(
  '[Pokemon] Load Pokemon Details Success',
  props<{ detailData: PokemonDetail }>()
);

export const loadPokemonDetailsFailure = createAction(
  '[Pokemon] Load Pokemon Details Failure',
  props<{ error: unknown }>()
);

// Types
export type PokemonActions =
  | ReturnType<typeof loadPokemons>
  | ReturnType<typeof loadPokemonsSuccess>
  | ReturnType<typeof loadPokemonsFailure>
  | ReturnType<typeof loadPokemonDetails>
  | ReturnType<typeof loadPokemonDetailsSuccess>
  | ReturnType<typeof loadPokemonDetailsFailure>;
