import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';

import { CustomMaterialModule } from '../../core/shared/custom-material.module';

import { PokemonListRoutingModule } from './pokemon-list-routing.module';

import { PokemonListComponent } from './pokemon-list.component';
import { PokemonCardComponent } from './components/pokemon-card/pokemon-card.component';
import { PokemonFilterComponent } from "./components/pokemon-filter/pokemon-filter.component";

import { FilterByName } from './pipes/filter-by-name';
import { SharedModule } from '../../shared/shared.module';
import {TranslocoModule} from "@ngneat/transloco";
import { PokemonDetailsDialogComponent } from "./components/pokemon-details-dialog/pokemon-details-dialog.component";

@NgModule({
  imports: [
    CustomMaterialModule,
    CommonModule,
    HttpClientModule,
    RouterModule,
    PokemonListRoutingModule,
    SharedModule,
    FormsModule,
    TranslocoModule
  ],
  declarations: [
    FilterByName,
    PokemonCardComponent,
    PokemonFilterComponent,
    PokemonListComponent,
    PokemonDetailsDialogComponent
  ],
})
export class PokemonListModule {}
