import { Component, Input, OnDestroy } from '@angular/core';
import { delay } from 'rxjs/operators';

import { Pokemon } from '../../../../core/models/pokemon.model';

import * as PokemonActions from '../../../../core/store/actions/pokemon.actions';

import { Store } from "@ngrx/store";
import { AppState } from "../../../../core/models/app.state";
import { Subscription } from "rxjs";
import { PokemonDetail } from "../../../../core/models/pokemon-detail.model";
import { PokemonDetailsDialogComponent } from "../pokemon-details-dialog/pokemon-details-dialog.component";
import { MatDialog } from "@angular/material/dialog";

@Component({
  selector: 'app-pokemon-card',
  templateUrl: './pokemon-card.component.html',
  styleUrls: ['./pokemon-card.component.scss'],
})
export class PokemonCardComponent implements OnDestroy {
  @Input() pokemon!: Pokemon;
  pokemonDetailsSubscription!: Subscription;
  pokemonDetails?: PokemonDetail;
  loaded = false;

  constructor(
    private store: Store<AppState>,
    public dialog: MatDialog
  ) {}

  // Método para buscar detalhes do Pokémon
  fetchDetails(id: number) {
    this.loaded = false;
    this.store.dispatch(PokemonActions.loadPokemonDetails({ id }));

    // Cancela qualquer inscrição anterior para evitar vazamento de memória
    if (this.pokemonDetailsSubscription) {
      this.pokemonDetailsSubscription.unsubscribe();
    }

    // Atualiza os detalhes do Pokémon no state
    // Adicionado atraso para exibir a frase de carregamento
    this.pokemonDetailsSubscription = this.store
      .select(state => state.pokemon.pokemonDetails[id])
      .pipe(
        delay(750)
      )
      .subscribe(details => {
        this.pokemonDetails = details;
        this.loaded = true;
      });
  }

  // Método para abrir um diálogo de detalhes do Pokémon
  openDialog(): void {
    this.dialog.open(PokemonDetailsDialogComponent, {
      width: '80%',
      data: { pokemonDetails: this.pokemonDetails }
    });
  }

  // Método para lidar com a destruição do componente
  ngOnDestroy() {
    if (this.pokemonDetailsSubscription) {
      this.pokemonDetailsSubscription.unsubscribe();
    }
  }
}
