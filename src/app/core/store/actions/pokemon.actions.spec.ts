import * as PokemonActions from './pokemon.actions';
import { PokemonListResponse } from '../../models/pokemon-list-response.model';

describe('Pokemon Actions', () => {
  /**
   * Teste: deve criar a ação loadPokemons.
   */
  describe('loadPokemons', () => {
    it('deve criar a ação', () => {
      const action = PokemonActions.loadPokemons();
      expect(action.type).toBe('[Pokemon] Load Pokemons');
    });
  });

  /**
   * Teste: deve criar a ação loadPokemonsSuccess com os dados fornecidos.
   */
  describe('loadPokemonsSuccess', () => {
    it('deve criar a ação com os dados fornecidos', () => {
      const mockData: PokemonListResponse = {
        count: 1,
        next: null,
        previous: null,
        results: [
          {
            id: 17,
            name: 'Pikachu',
            sprite: 'https://some-img-url.com/pikachu.png',
            url: 'https://pokeapi.co/pokemon/25/',
          },
        ],
      };
      const action = PokemonActions.loadPokemonsSuccess({ data: mockData });
      expect(action.type).toBe('[Pokemon] Load Pokemons Success');
      expect(action.data).toEqual(mockData);
    });
  });

  /**
   * Teste: deve criar a ação loadPokemonsFailure com o erro fornecido.
   */
  describe('loadPokemonsFailure', () => {
    it('deve criar a ação com o erro fornecido', () => {
      const mockError = new Error('Ocorreu um erro');
      const action = PokemonActions.loadPokemonsFailure({ error: mockError });
      expect(action.type).toBe('[Pokemon] Load Pokemons Failure');
      expect(action.error).toEqual(mockError);
    });
  });
});
