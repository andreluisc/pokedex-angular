import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

// Páginas
import { PageNotFoundComponent } from './core/components/page-not-found/page-not-found.component';

/**
 * Módulo de configuração das rotas da aplicação.
 */
const routes: Routes = [
  {
    path: '',
    loadChildren: () =>
      import('./features/pokemon-list/pokemon-list.module').then(
        (c) => c.PokemonListModule
      ),
  },
  // {
  //   path: ':id',
  //   loadChildren: () =>
  //     import('./modules/pokemon/pokemon.module').then((c) => c.PokemonModule),
  // },
  {
    path: '**',
    component: PageNotFoundComponent,
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
