import { Pipe, PipeTransform } from '@angular/core';

// Define um pipe personalizado chamado 'capitalize'
@Pipe({
  name: 'capitalize'
})
export class CapitalizePipe implements PipeTransform {

  // Implementa o método 'transform' da interface PipeTransform
  transform(value: string | null | undefined): string {
    // Verifica se o valor de entrada é nulo ou indefinido e retorna uma string vazia se for o caso
    if (!value) return '';

    // Transforma a primeira letra da string em maiúscula e mantém o restante da string inalterado
    return value.charAt(0).toUpperCase() + value.slice(1);
  }

}
