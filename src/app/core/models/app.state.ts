// Esta interface representa o estado global da sua aplicação,
// incluindo todos os estados específicos do domínio, como o estado dos pokémons.
import { PokemonState } from './pokemon-state.model';

export interface AppState {
  // O estado do módulo Pokémon é armazenado na propriedade 'pokemon'.
  pokemon: PokemonState;
}
