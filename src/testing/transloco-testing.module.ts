import { TranslocoTestingModule, TranslocoTestingOptions } from '@ngneat/transloco';

import en from '../assets/i18n/en.json';
import ptBR from '../assets/i18n/pt-BR.json'

export function getTranslocoModule(options: TranslocoTestingOptions = {}) {
  return TranslocoTestingModule.forRoot({
    langs: { en, ptBR },
    translocoConfig: {
      availableLangs: ['en', 'ptBR'],
      defaultLang: 'ptBR',
    },
    preloadLangs: true,
    ...options
  });
}
