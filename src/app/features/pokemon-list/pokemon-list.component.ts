import {Component, OnInit, OnDestroy, ViewChild} from '@angular/core';
import { Store } from '@ngrx/store';
import { Subscription } from 'rxjs';
import { PageEvent } from '@angular/material/paginator';

import { Pokemon } from '../../core/models/pokemon.model';
import { getPokeImageUrl } from '../../core/utils/pokemon-image.util';
import * as PokemonActions from '../../core/store/actions/pokemon.actions';
import { AppState } from '../../core/models/app.state';

import { MatPaginator } from '@angular/material/paginator';



@Component({
  selector: 'app-pokemon-list',
  templateUrl: './pokemon-list.component.html',
  styleUrls: ['./pokemon-list.component.scss'],
})
export class PokemonListComponent implements OnInit, OnDestroy {
  filter = '';
  pokemons: Pokemon[] = [];
  displayedPokemons: Pokemon[] = [];
  totalPokemonCount = 0;
  pageSize = 9;
  isLoading = false;
  error = false;
  subscriptions: Subscription[] = [];
  pokemons$ = this.store.select((state) => state.pokemon.data);

  @ViewChild(MatPaginator, { static: false }) paginator!: MatPaginator;

  constructor(private store: Store<AppState>) {}

  ngOnInit(): void {
    // Carrega os Pokémon ao inicializar o componente
    this.store.dispatch(PokemonActions.loadPokemons());
    this.retrievePokemons();
  }

  retrievePokemons(): void {
    // Inscreve-se para obter Pokémon da loja
    this.subscriptions.push(
      this.store
        .select((state) => state.pokemon.data)
        .subscribe((pokemons) => {
          if (pokemons) {
            this.pokemons = pokemons.results.map((pokemon) =>
              this.mapPokemonData(pokemon)
            );
            this.updateDisplayedPokemons(0);
          }
        })
    );

    // Inscreve-se para monitorar o estado de carregamento da loja
    this.subscriptions.push(
      this.store
        .select((state) => state.pokemon.loading)
        .subscribe((isLoading) => {
          this.isLoading = isLoading;
        })
    );

    // Inscreve-se para monitorar erros provenientes da loja
    this.subscriptions.push(
      this.store
        .select((state) => state.pokemon.error)
        .subscribe((error) => {
          this.error = !!error;
        })
    );
  }

  private mapPokemonData(pokemon: Pokemon): Pokemon {
    // Mapeia os dados brutos do Pokémon para um formato útil para o componente
    const parts = pokemon.url.split('/');
    const id = Number(parts[parts.length - 2]);
    return {
      ...pokemon,
      id: id,
      sprite: getPokeImageUrl(id),
    };
  }

  updateDisplayedPokemons(startIndex: number): void {
    // Filter the Pokémon based on the filter text
    const filteredPokemons = this.pokemons.filter(pokemon => pokemon.name?.toLowerCase().includes(this.filter));

    // update the total count
    this.totalPokemonCount = filteredPokemons.length;

    // Slice the Pokémon list for pagination after filtering
    this.displayedPokemons = filteredPokemons.slice(
      startIndex,
      startIndex + this.pageSize
    );
  }

  pageChanged(event: PageEvent): void {
    // Manipula mudanças de página
    const startIndex = event.pageIndex * event.pageSize;
    this.pageSize = event.pageSize;
    this.updateDisplayedPokemons(startIndex);
  }

  applyFilter(filterValue: string) {
    this.filter = filterValue.toLowerCase();
    this.updateDisplayedPokemons(0);
    console.log('aqui')
    this.paginator.pageIndex = 0
  }

  ngOnDestroy(): void {
    // Garante a limpeza das inscrições quando o componente é destruído
    this.subscriptions.forEach((sub) => sub.unsubscribe());
  }
}
