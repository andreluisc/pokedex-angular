import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { PokemonListResponse } from '../models/pokemon-list-response.model';
import { environment } from '../../../environments/environment';
import {RawPokemonDetail} from "../models/pokemon-detail.model";

/**
 * @class
 * Serviço para recuperar uma lista de Pokémon com um deslocamento e limite especificados.
 */
@Injectable({
  providedIn: 'root',
})
export class PokemonService {
  constructor(private http: HttpClient) {}

  /**
   * Recupera uma lista de Pokémon com um deslocamento e limite especificados.
   * @param offset O índice de início da lista.
   * @param limit O número máximo de Pokémon a serem recuperados.
   * @returns Um Observable do tipo PokemonListResponse.
   */
  getAll(
    offset = 0,
    limit = 2000
  ): Observable<PokemonListResponse> {
    const url = `${environment.pokeApiBaseUrl}?offset=${offset}&limit=${limit}`;
    return this.http.get<PokemonListResponse>(url);
  }

  getDetails(id: number | undefined) {
    return this.http.get<RawPokemonDetail>(`https://pokeapi.co/api/v2/pokemon/${id}/`);
  }
}

