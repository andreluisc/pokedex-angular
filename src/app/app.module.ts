import { NgModule, APP_INITIALIZER } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';
import { APP_BASE_HREF } from '@angular/common';

/**
 * O serviço HTTP não é adicionado por padrão, portanto, precisamos importá-lo.
 */
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CustomMaterialModule } from './core/shared/custom-material.module';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavigationComponent } from './core/components/navigation/navigation.component';
import { SharedModule } from './shared/shared.module';
import { TranslocoRootModule } from './transloco-root.module';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';

import { TranslationsLoaderService } from './core/services/translation/translations-loader.service';

/**
 * Componentes da rota
 */
import { PageNotFoundComponent } from './core/components/page-not-found/page-not-found.component';
import { PokemonStoreModule } from './core/store/store.module';

@NgModule({
  declarations: [AppComponent, NavigationComponent, PageNotFoundComponent],
  imports: [
    BrowserModule,
    RouterModule.forRoot([]),
    BrowserAnimationsModule,
    CustomMaterialModule,
    AppRoutingModule,
    SharedModule,
    HttpClientModule,
    TranslocoRootModule,

    StoreModule.forRoot({}),
    EffectsModule.forRoot([]),
    PokemonStoreModule,
  ],
  providers: [
    {
      provide: APP_BASE_HREF,
      useValue: '/',
    },
    {
      provide: APP_INITIALIZER,
      useFactory: (translationLoader: TranslationsLoaderService) => () =>
        translationLoader.preloadTranslations(),
      deps: [TranslationsLoaderService],
      multi: true,
    },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
