import { getPokeImageUrl } from "../utils/pokemon-image.util";

// Interface que define uma habilidade de Pokémon
export interface PokemonAbility {
  name: string;
}

// Interface que define um movimento de Pokémon
export interface PokemonMove {
  name: string;
}

// Interface que define um stat (atributo) de Pokémon
export interface PokemonStat {
  name: string;
  stat: number;
}

// Interface que define o formato bruto (raw) dos dados de Pokémon
interface RawPokemonAbility {
  ability: {
    name: string;
  };
}

interface RawPokemonMove {
  move: {
    name: string;
  };
}

interface RawPokemonStat {
  base_stat: number;
  stat: {
    name: string;
  };
}

interface RawPokemonType {
  type: {
    name: string;
  };
}

// Interface que define o formato bruto (raw) dos detalhes do Pokémon
export interface RawPokemonDetail {
  id: number;
  name: string;
  abilities: RawPokemonAbility[];
  height: number;
  moves: RawPokemonMove[];
  stats: RawPokemonStat[];
  types: RawPokemonType[];
  weight: number;
}

// Classe que representa os detalhes de um Pokémon
export class PokemonDetail {
  id: number;
  name: string;
  sprite: string;
  abilities: PokemonAbility[];
  height: number;
  moves: PokemonMove[];
  stats: PokemonStat[];
  types: string[];
  weight: number;

  constructor(data: RawPokemonDetail) {
    this.id = data.id;
    this.name = data.name;
    this.sprite = getPokeImageUrl(data.id); // Obtém a URL da imagem do Pokémon
    this.abilities = data.abilities.map(ability => ({ name: ability.ability.name }));
    this.height = data.height / 10; // Converte a altura para metros
    this.moves = data.moves.map(move => ({ name: move.move.name }));
    this.stats = data.stats.map(stat => ({ name: stat.stat.name, stat: stat.base_stat }));
    this.types = data.types.map(type => type.type.name);
    this.weight = data.weight / 10; // Converte o peso para quilogramas
  }
}
