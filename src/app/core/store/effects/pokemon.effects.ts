import { Injectable } from '@angular/core';
import { Actions, ofType, createEffect } from '@ngrx/effects';
import {catchError, map, mergeMap, withLatestFrom} from 'rxjs/operators';
import { of } from 'rxjs';
import { Store } from '@ngrx/store';

import * as pokemonActions from '../actions/pokemon.actions';
import { PokemonService } from '../../services/pokemon.service';
import { PokemonDetail } from "../../models/pokemon-detail.model";
import {AppState} from "../../models/app.state";

@Injectable()
export class PokemonEffects {
  // Effect para lidar com a ação de carregamento de pokémons.
  loadPokemons$ = createEffect(() =>
    this.actions$.pipe(
      ofType(pokemonActions.loadPokemons),
      mergeMap(() =>
        this.pokemonService.getAll().pipe(
          // Map da resposta de sucesso para uma ação de sucesso.
          map((data) => pokemonActions.loadPokemonsSuccess({ data })),
          // Em caso de erros
          catchError((error) =>
            of(pokemonActions.loadPokemonsFailure({ error }))
          )
        )
      )
    )
  );

  loadPokemonDetails$ = createEffect(() =>
    this.actions$.pipe(
      ofType(pokemonActions.loadPokemonDetails),
      withLatestFrom(this.store.select(state => state.pokemon.pokemonDetails)),
      mergeMap(([action, pokemonDetails]) => {
        if (pokemonDetails[action.id]) {
          // If the details are already loaded, just return them.
          return of(pokemonActions.loadPokemonDetailsSuccess({ detailData: pokemonDetails[action.id] }));
        }

        return this.pokemonService.getDetails(action.id).pipe(
          map(detailData => {
            const parsedData = new PokemonDetail(detailData);
            console.log('parsedData', parsedData)
            return pokemonActions.loadPokemonDetailsSuccess({ detailData: parsedData });
          }),
          catchError(error => of(pokemonActions.loadPokemonDetailsFailure({ error })))
        );
      })
    )
  );

  constructor(
    private actions$: Actions,
    private pokemonService: PokemonService,
    private store: Store<AppState>
  ) {}
}
