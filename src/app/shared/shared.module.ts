import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AsyncImageComponent } from './components/async-image/async-image.component';
import { CustomMaterialModule } from '../core/shared/custom-material.module';
import { CapitalizePipe } from "./pipes/capitalize/capitalize.pipe";

@NgModule({
  declarations: [AsyncImageComponent, CapitalizePipe],
  imports: [CommonModule, CustomMaterialModule],
  exports: [AsyncImageComponent, CapitalizePipe],
})
export class SharedModule {}
