import { Component, Input } from '@angular/core';

/**
 * Componente que exibe uma imagem de forma assíncrona, permitindo a definição
 * da URL da imagem e do texto para a propriedade alt.
 */
@Component({
  selector: 'app-async-image',
  templateUrl: './async-image.component.html',
  styleUrls: ['./async-image.component.scss'],
})
export class AsyncImageComponent {
  /**
   * URL imagem que será carregada
   */
  @Input() src: string | undefined = '';

  /**
   * Texto da propriedade ALT
   */
  @Input() alt: string | undefined = '';

  /**
   * Uma flag que indica se a imagem foi carregada com sucesso.
   */
  isLoaded = false;
}
