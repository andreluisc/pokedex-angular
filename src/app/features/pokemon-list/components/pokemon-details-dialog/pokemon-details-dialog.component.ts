import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { PokemonDetail } from '../../../../core/models/pokemon-detail.model';

@Component({
  selector: 'app-pokemon-details-dialog',
  templateUrl: './pokemon-details-dialog.component.html',
  styleUrls: ['./pokemon-details-dialog.component.scss'],
})
export class PokemonDetailsDialogComponent {
  constructor(@Inject(MAT_DIALOG_DATA) public data: { pokemonDetails: PokemonDetail }) { }

}
