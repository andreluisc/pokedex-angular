import { NgModule } from '@angular/core';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { pokemonReducer } from './reducers/pokemon.reducer'; // Assuming you have a reducer created
import { PokemonEffects } from './effects/pokemon.effects';

@NgModule({
  imports: [
    StoreModule.forFeature('pokemon', pokemonReducer),
    EffectsModule.forFeature([PokemonEffects]),
  ],
})
export class PokemonStoreModule {}
