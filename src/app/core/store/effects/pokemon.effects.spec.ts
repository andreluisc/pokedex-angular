import { TestBed } from '@angular/core/testing';
import { provideMockActions } from '@ngrx/effects/testing';
import { ReplaySubject, of, throwError } from 'rxjs';
import { PokemonEffects } from './pokemon.effects';
import * as pokemonActions from '../actions/pokemon.actions';
import { PokemonService } from '../../services/pokemon.service';
import { provideMockStore } from '@ngrx/store/testing';

describe('PokemonEffects', () => {
  let actions$: ReplaySubject<pokemonActions.PokemonActions>;
  let effects: PokemonEffects;
  let pokemonService: jest.Mocked<PokemonService>;

  beforeEach(() => {
    actions$ = new ReplaySubject(1);

    const mockPokemonService = {
      getAll: jest.fn(),
    };

    TestBed.configureTestingModule({
      providers: [
        PokemonEffects,
        provideMockActions(() => actions$),
        { provide: PokemonService, useValue: mockPokemonService },
        provideMockStore({ initialState: {} })
      ],
    });

    effects = TestBed.inject(PokemonEffects);
    pokemonService = TestBed.inject(
      PokemonService
    ) as jest.Mocked<PokemonService>;

  });

  /**
   * Teste: deve retornar uma ação loadPokemonsSuccess em uma chamada de API bem-sucedida.
   */
  it('deve retornar uma ação loadPokemonsSuccess em uma chamada de API bem-sucedida', (done) => {
    const mockData = {
      count: 10,
      next: null,
      previous: null,
      results: [
        {
          id: 17,
          name: 'Pikachu',
          sprite: 'https://some-img-url.com/pikachu.png',
          url: 'https://pokeapi.co/pokemon/25/',
        },
      ],
    };

    pokemonService.getAll.mockReturnValue(of(mockData));

    actions$.next(pokemonActions.loadPokemons());

    effects.loadPokemons$.subscribe((action) => {
      expect(action).toEqual(
        pokemonActions.loadPokemonsSuccess({ data: mockData })
      );
      done();
    });
  });

  /**
   * Teste: deve retornar uma ação loadPokemonsFailure em uma chamada de API com falha.
   */
  it('deve retornar uma ação loadPokemonsFailure em uma chamada de API com falha', (done) => {
    const mockError = new Error('Falha na chamada de API');

    pokemonService.getAll.mockReturnValue(throwError(mockError));

    actions$.next(pokemonActions.loadPokemons());

    effects.loadPokemons$.subscribe((action) => {
      expect(action).toEqual(
        pokemonActions.loadPokemonsFailure({ error: mockError })
      );
      done();
    });
  });

  afterEach(() => {
    actions$.complete();
  });
});
