module.exports = {
  rootTranslationsPath: 'src/assets/i18n/',
  availableLangs: ['pt-BR', 'en'],
  defaultLang: 'pt-BR',
  fallbackLang: 'en',
  reRenderOnLangChange: true,
};
