import { CapitalizePipe } from './capitalize.pipe';

describe('CapitalizePipe', () => {

  let pipe: CapitalizePipe;

  // Instancia o pipe antes de cada teste
  beforeEach(() => {
    pipe = new CapitalizePipe();
  });

  /**
   * Teste: deve capitalizar a primeira letra de uma palavra
   */
  it('deve capitalizar a primeira letra de uma palavra', () => {
    expect(pipe.transform('angular')).toBe('Angular');
    expect(pipe.transform('CAPITALIZE')).toBe('CAPITALIZE'); // já capitalizado
    expect(pipe.transform('a')).toBe('A'); // uma única letra
  });

  /**
   * Teste: deve retornar a string original se ela tiver números ou caracteres especiais como o primeiro caractere
   */
  it('deve retornar a string original se ela tiver números ou caracteres especiais como o primeiro caractere', () => {
    expect(pipe.transform('1angular')).toBe('1angular');
    expect(pipe.transform('@angular')).toBe('@angular');
  });

  /**
   * Teste: deve retornar uma string vazia se o valor for nulo ou indefinido
   */
  it('deve retornar uma string vazia se o valor for nulo ou indefinido', () => {
    expect(pipe.transform(null)).toBe('');
    expect(pipe.transform(undefined)).toBe('');
  });

  /**
   * Teste: não deve modificar o restante da string
   */
  it('não deve modificar o restante da string', () => {
    expect(pipe.transform('aNGULAR')).toBe('ANGULAR');
  });
});
