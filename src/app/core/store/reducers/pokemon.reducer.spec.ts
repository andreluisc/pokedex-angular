import * as PokemonActions from '../actions/pokemon.actions';
import { pokemonReducer } from './pokemon.reducer';
import { initialPokemonState } from '../../models/pokemon-state.model';

describe('pokemonReducer', () => {
  /**
   * Teste: ação loadPokemons
   */
  describe('loadPokemons action', () => {
    it('deve definir loading como true', () => {
      const action = PokemonActions.loadPokemons();
      const state = pokemonReducer(initialPokemonState, action);

      expect(state.loading).toBe(true);
      expect(state.loaded).toBe(false);
    });
  });

  /**
   * Teste: ação loadPokemonsSuccess
   */
  describe('loadPokemonsSuccess action', () => {
    it('deve preencher os dados e definir loading como false e loaded como true', () => {
      const mockData = {
        count: 10,
        next: null,
        previous: null,
        results: [
          {
            id: 17,
            name: 'Pikachu',
            sprite: 'https://some-img-url.com/pikachu.png',
            url: 'https://pokeapi.co/pokemon/25/',
          },
        ],
      };

      const action = PokemonActions.loadPokemonsSuccess({ data: mockData });
      const state = pokemonReducer(initialPokemonState, action);

      expect(state.data).toEqual(mockData);
      expect(state.loading).toBe(false);
      expect(state.loaded).toBe(true);
    });
  });

  /**
   * Teste: ação loadPokemonsFailure
   */
  describe('loadPokemonsFailure action', () => {
    it('deve preencher o erro e definir loading como false e loaded como false', () => {
      const mockError = new Error('Ocorreu um erro');

      const action = PokemonActions.loadPokemonsFailure({ error: mockError });
      const state = pokemonReducer(initialPokemonState, action);

      expect(state.error).toEqual(mockError);
      expect(state.loading).toBe(false);
      expect(state.loaded).toBe(false);
    });
  });
});
