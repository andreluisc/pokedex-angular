import { ComponentFixture, TestBed } from '@angular/core/testing';
import { PokemonFilterComponent } from './pokemon-filter.component';

describe('PokemonFilterComponent', () => {
  let component: PokemonFilterComponent;
  let fixture: ComponentFixture<PokemonFilterComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [PokemonFilterComponent],

    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PokemonFilterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('componente deve ser criado', () => {
    expect(component).toBeTruthy();
  });

  it('should emit filter text on input change', () => {
    const emitSpy = jest.spyOn(component.filterChanged, 'emit');
    const filterText = 'Pikachu';

    component.filterText = filterText;
    component.onInputChange();

    expect(emitSpy).toHaveBeenCalledWith(filterText);
  });
});
