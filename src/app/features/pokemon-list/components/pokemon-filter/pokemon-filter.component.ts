import { Component, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-pokemon-filter',
  templateUrl: './pokemon-filter.component.html',
  styleUrls: ['./pokemon-filter.component.scss'],
})
export class PokemonFilterComponent {
  filterText = '';

  @Output() filterChanged = new EventEmitter<string>();

  onInputChange() {
    this.filterChanged.emit(this.filterText);
  }
}
