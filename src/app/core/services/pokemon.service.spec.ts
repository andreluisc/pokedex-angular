import { TestBed } from '@angular/core/testing';
import {
  HttpClientTestingModule,
  HttpTestingController,
} from '@angular/common/http/testing';
import { PokemonService } from './pokemon.service';
import { environment } from '../../../environments/environment';

describe('PokemonListService', () => {
  let service: PokemonService;
  let httpTestingController: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [PokemonService],
    });
    service = TestBed.inject(PokemonService);
    httpTestingController = TestBed.inject(HttpTestingController);
  });

  afterEach(() => {
    httpTestingController.verify();
  });

  /**
   * Testa se o serviço foi criado com sucesso.
   */
  it('componente deve ser criado', () => {
    expect(service).toBeTruthy();
  });

  /**
   * Testa se é possível recuperar uma lista de Pokémon.
   */
  it('deve recuperar uma lista de Pokémon', () => {
    const mockResponse = {
      count: 1292,
      next: null,
      previous: null,
      results: [
        {
          name: 'bulbasaur',
          url: 'https://pokeapi.co/api/v2/pokemon/1/',
        },
        {
          name: 'ivysaur',
          url: 'https://pokeapi.co/api/v2/pokemon/2/',
        },
      ],
    };
    const offset = 0;
    const limit = 2000;

    service.getAll(offset, limit).subscribe((response) => {
      expect(response).toEqual(mockResponse);
    });

    const req = httpTestingController.expectOne(
      `${environment.pokeApiBaseUrl}?offset=${offset}&limit=${limit}`
    );

    expect(req.request.method).toEqual('GET');

    req.flush(mockResponse);
  });
});
