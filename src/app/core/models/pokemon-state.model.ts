import { PokemonListResponse } from './pokemon-list-response.model';
import { PokemonDetail } from "./pokemon-detail.model";

/**
 * @interface
 * Representa o estado relacionado aos Pokémon na aplicação, incluindo dados carregados,
 * status de carregamento e possíveis erros.
 */
export interface PokemonState {
  /**
   * Os dados dos Pokémon, que podem ser nulos se ainda não foram carregados.
   */
  data: PokemonListResponse | null;

  /**
   * Uma flag que indica se os dados dos Pokémon foram carregados com sucesso.
   */
  loaded: boolean;

  /**
   * Uma flag que indica se a aplicação está atualmente carregando os dados dos Pokémon.
   */
  loading: boolean;

  /**
   * Qualquer erro que tenha ocorrido ao carregar ou manipular os dados dos Pokémon.
   */
  error: unknown;

  pokemonDetails: { [id: number]: PokemonDetail };
}

/**
 * O estado inicial para o módulo de Pokémon, com todas as propriedades definidas como valores padrão.
 */
export const initialPokemonState: PokemonState = {
  data: null,
  loaded: false,
  loading: false,
  error: null,
  pokemonDetails: {}
};
