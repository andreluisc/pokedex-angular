export const environment = {
  production: true,
  pokeApiBaseUrl: 'https://pokeapi.co/api/v2/pokemon',
  pokeApiThumbImageUrl: 'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/',
  pokeApiImageUrl: 'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/'
};
