import { Injectable } from '@angular/core';
import { TranslocoService } from '@ngneat/transloco';

@Injectable({
  providedIn: 'root',
})
export class TranslationsLoaderService {
  constructor(private transloco: TranslocoService) {}

  preloadTranslations(): Promise<Record<string, unknown>> {
    return this.transloco.load('pt-BR').toPromise() as Promise<Record<string, unknown>>;
  }
}
