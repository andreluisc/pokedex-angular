import { environment } from '../../../environments/environment';

export function getPokeImageUrl(id: number): string {
  return `${environment.pokeApiImageUrl}${id}.png`;
}

export function getPokeThumbImageUrl(id: number): string {
  return `${environment.pokeApiThumbImageUrl}${id}.png`;
}
